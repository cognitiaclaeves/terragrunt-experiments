
output "vpc_cidr_block" {
  value = module.vpc.vpc_cidr_block
}

output "vpc_id" {
  value = module.vpc.vpc_id
}

output "management_sg" {
  value = module.management_sg
}

output "ansible_sg" {
  value = module.ansible_sg
}
