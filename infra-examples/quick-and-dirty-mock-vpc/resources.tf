
# This module can be used to create a real VPC.
# But I'm only creating a 'mock' VPC with fewer resources that generate cost.

data "aws_ec2_managed_prefix_list" "me" {
  name   = "me"
}

module "vpc" {
  source = "github.com/terraform-aws-modules/terraform-aws-vpc.git"

  name = var.vpc_name
  cidr = var.cidr

  azs             = var.azs
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  # keep resources free
  enable_nat_gateway = false
  enable_vpn_gateway = false

  create_igw         = var.create_igw

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

module "management_sg" {
  source = "github.com/terraform-aws-modules/terraform-aws-security-group.git"

  name        = "devops-management"
  description = "Security group for instance management (ssh and RDP) within vpc"
  vpc_id      = module.vpc.vpc_id

  # Use a prefix list for my ip address. Just need to change it in one place if it changes.
  ingress_prefix_list_ids = [ data.aws_ec2_managed_prefix_list.me.id ]
  ingress_with_cidr_blocks = [
    {
      rule = "rdp-tcp",
      cidr_blocks = module.vpc.vpc_cidr_block
    },
    {
      rule = "rdp-udp",
      cidr_blocks = module.vpc.vpc_cidr_block
    },
    {
      rule = "ssh-tcp",
      cidr_blocks = module.vpc.vpc_cidr_block
    }    
  ]
  # this is needed for SSM agent to contact AWS
  egress_with_cidr_blocks = [
    {
      rule = "https-443-tcp",
      cidr_blocks = "0.0.0.0/0"
    },
    {
      rule = "http-80-tcp",
      cidr_blocks = "0.0.0.0/0"
    }

    # {
    #   rule = "https-udp",
    #   cidr_blocks = module.vpc.vpc_cidr_block
    # },
    # {
    #   rule = "http-udp",
    #   cidr_blocks = module.vpc.vpc_cidr_block
    # },
  ]
}

module "ansible_sg" {
  source = "github.com/terraform-aws-modules/terraform-aws-security-group.git"

  name        = "ansible-management"
  description = "Security group for ansible management within VPC"
  vpc_id      = module.vpc.vpc_id

  # Use a prefix list for my ip address. Just need to change it in one place if it changes.
  ingress_prefix_list_ids = [ data.aws_ec2_managed_prefix_list.me.id ]
  ingress_with_cidr_blocks = [
    {
      rule = "ssh-tcp",
      cidr_blocks = module.vpc.vpc_cidr_block
    },
    {
      rule = "winrm-http-tcp",
      cidr_blocks = module.vpc.vpc_cidr_block
    },
    {
      rule = "winrm-https-tcp",
      cidr_blocks = module.vpc.vpc_cidr_block
    }
  ]
}
