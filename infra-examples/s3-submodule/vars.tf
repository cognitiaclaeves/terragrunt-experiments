variable "buckets" {
  description = "module variable for buckets"
  type        = any
}

variable "default_web_bucket_config" {
  default = {
    noncurrent_retention = {
       days = 1
       id = "expire-prior-after-1-day"
    }
    current_retention = []
    custom_tags = {}
  }
}

variable "default_incomplete_multipart_upload_days" {
  default = "7"
}

# tg_vars passed through here from other modules
# or via more awkward direct call from TG infra.

variable "passthrough_vars" {
  type = any
  default = {}
}
