output "passthrough_vars" {
  value = var.passthrough_vars
}

output "buckets" {
  value = var.buckets
}