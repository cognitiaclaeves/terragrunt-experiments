# Creates one s3 bucket for every bucket passed in map var.buckets 
# (see caller (or terragrunt-infra tg-alt/../buckets-json/module.bucket.tfvars.json) for layout)

locals {
  tf_tags = {
    owner = "owner"
    managed_by = "terragrunt"
    purpose = "todo: purpose should be passed with bucket spec"
    env = var.passthrough_vars.env_vars.env
  }
}

# buckets: versioning + life-cycle
resource "aws_s3_bucket_public_access_block" "s3_bucket_public_restriction" {
  for_each = var.buckets
  # currently only supports full block or full unblocked
  bucket = aws_s3_bucket.s3_buckets[each.key].id
  # Block public access to buckets and objects granted through new access control lists (ACLs)
  block_public_acls = var.buckets[each.key].block_public_access
  # Block public access to buckets and objects granted through any access control lists (ACLs)
  ignore_public_acls = var.buckets[each.key].block_public_access
  # Block public access to buckets and objects granted through new public bucket or access point policies
  block_public_policy = var.buckets[each.key].block_public_access
  # Block public and cross-account access to buckets and objects through any public bucket or access point policies
  restrict_public_buckets = var.buckets[each.key].block_public_access
}

resource "aws_s3_bucket" "s3_buckets" {
  for_each = var.buckets
  bucket = var.buckets[each.key].bucket_name
  acl    = var.buckets[each.key].acl
 
  tags = merge(local.tf_tags, var.buckets[each.key].custom_tags)
  # versioning needed for replication
  versioning {
    enabled = var.buckets[each.key].versioning
  }
  # lifecycle highly recommended when versioning
  # so, if versioning is true, will require lifecycle settings
  # otherwise, will not look for them. 
  # (Can be changed later if lifecyle is determined to be needed without versioning.)
  dynamic "lifecycle_rule" {
    for_each = var.buckets[each.key].versioning == false ? [] : var.buckets[each.key].current_retention 
    content {
      enabled = true
      id = lifecycle_rule.value["id"]
      abort_incomplete_multipart_upload_days = var.default_incomplete_multipart_upload_days
      expiration {
        days = lifecycle_rule.value["days"]
        # note on expired_object_delete_marker:
        # days = var.data_bucket_map[each.key].current_retention[0].days
        # expired_object_delete_marker = true
        # it seems like the above can't be enabled if expiration is enabled
      }
    }
  }
  dynamic "lifecycle_rule" {
    for_each = var.buckets[each.key].versioning == false ? [] : var.buckets[each.key].noncurrent_retention 
    content {
      enabled = true
      id = lifecycle_rule.value["id"]
      abort_incomplete_multipart_upload_days = var.default_incomplete_multipart_upload_days
      noncurrent_version_expiration {
        days = lifecycle_rule.value["days"]
      }
    }
  }

}
