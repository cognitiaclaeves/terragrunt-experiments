
module "generic-s3" {
  # source = "git@gitlab.com:cognitiaclaeves/terragrunt-experiments.git//infra-examples/s3-submodule?ref=master"
  source = "git::https://gitlab.com/cognitiaclaeves/terragrunt-experiments.git//infra-examples/s3-submodule?ref=master"

  buckets = local.bucket_spec
  passthrough_vars = var.tg_vars
}
