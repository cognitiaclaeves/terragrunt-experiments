
# Example of a possible "thing" that might need a bucket:

# Cloud-fronted S3 bucket home for javascript front-end:
# creates one instance of each for every service-name passed in list var.services_names:
# - s3 bucket - uses module terragrunt-modules s3-json
# - CloudFront + origin access identity + appropriate permissions for bucket
# -    ( this cloudfronted-frontend creates the buckets as private )
# - Route53 record

locals{
  env = var.tg_vars.env_vars.env
  long_env = var.tg_vars.env_vars.environment

  bucket_spec = {
    for svcnm in var.service_names:
    svcnm => { 
      bucket_name         = "xxx-${svcnm}"
      acl                 = "private"
      block_public_access = true
      versioning          = false
      custom_tags         = { purpose = "frontend bucket for ${svcnm}" }
    }
  }
}
